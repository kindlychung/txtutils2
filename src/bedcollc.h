#ifndef BEDCOLL_H
#define BEDCOLL_H

#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <math.h> // ceil
#include <boost/filesystem.hpp>              // boost::filesystem::path .filename() .branch_path() ...
#include <boost/format.hpp>                  // boost::format
#include "countlines.h"                      // countlines()
#include "fileSize.h"                        // fileSize()
#include "collgen_memo.h"

typedef struct {
    size_t nsnp = 0;
    size_t nindiv = 0;
    size_t bytes_snp = 0;
    size_t bytes_read = 0;
    size_t bed_filesize = 0;
    size_t snp_iter = 5000;

    boost::filesystem::path fstem;
    boost::filesystem::path fbranch;
    std::string bedfn;
    std::string bimfn;
    std::string famfn;
    bool allinram = false;

    FILE *file_in = 0;
    FILE *outfile = 0;
} bedcoll;

bedcoll bedcollinit(std::string pFn, bool pAllinram);
void collapseSingleShift(bedcoll pBedcoll, size_t nshift);
void collapseSeqShift(bedcoll pBedcoll, size_t nshift_start, size_t nshift_end);

#endif // BEDCOLL_H
