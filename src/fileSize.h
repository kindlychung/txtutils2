#pragma once

#include <stdio.h>
#include <sys/stat.h>
#include <string>
#include "armaheader.h"
off_t fileSize(std::string fn);
