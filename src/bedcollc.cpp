#include "bedcollc.h"



bedcoll bedcollinit(std::string pFn, bool pAllinram) {
    bedcoll sBedcoll;
    sBedcoll.allinram = pAllinram;
    boost::filesystem::path fpath(pFn);
    sBedcoll.fstem = fpath.filename();
    sBedcoll.fbranch = fpath.branch_path();
    auto fbed = sBedcoll.fbranch / (sBedcoll.fstem.string() + ".bed");
    auto fbim = sBedcoll.fbranch / (sBedcoll.fstem.string() + ".bim");
    auto ffam = sBedcoll.fbranch / (sBedcoll.fstem.string() + ".fam");
    sBedcoll.bedfn = fbed.string();
    sBedcoll.bimfn = fbim.string();
    sBedcoll.famfn = ffam.string();
    sBedcoll.nsnp = countlines(sBedcoll.bimfn);
    sBedcoll.nindiv = countlines(sBedcoll.famfn);
    sBedcoll.bytes_snp = ceil(sBedcoll.nindiv / 4.0);
    sBedcoll.bytes_read = sBedcoll.bytes_snp * sBedcoll.nsnp;
    sBedcoll.bed_filesize = fileSize(sBedcoll.bedfn);

    if (sBedcoll.bed_filesize != (sBedcoll.bytes_read + 3)) {
        perror("File size of .bed file does not match with .bim and .fam");
        exit(EXIT_FAILURE);
    }

    sBedcoll.file_in = fopen(sBedcoll.bedfn.c_str(), "rb");
    if (!sBedcoll.file_in) {
        perror("Failed to read input bed file.");
        exit(EXIT_FAILURE);
    }

    return sBedcoll;
}

void collapseSingleShift(bedcoll pBedcoll, size_t nshift) {

    unsigned char *res_buffer = 0;
    unsigned char *buffer = 0;
    unsigned char *res_buffer_remain = 0;
    unsigned char *remain_buffer = 0;
    unsigned char *collres = 0;

    size_t bytes_shift = pBedcoll.bytes_snp * nshift;
    size_t bytes_left = pBedcoll.bytes_read - bytes_shift;
    size_t nsnp_left = pBedcoll.nsnp - nshift;

    boost::format outf_leaf("_shift_%04d.bed");
    outf_leaf % (int)nshift;
    auto fout = pBedcoll.fbranch / (pBedcoll.fstem.string() + outf_leaf.str());
    auto outfn = fout.string();

    if (pBedcoll.allinram == true) {
        // see the documentation picture on flickr
        buffer = (unsigned char *)malloc(pBedcoll.bytes_read);
        if (!buffer) {
            fprintf(stderr, "Failed to allocated memory for bed file.");
            fclose(pBedcoll.file_in);
            exit(EXIT_FAILURE);
        }
        fseeko(pBedcoll.file_in, 3, SEEK_SET);
        fread(buffer, pBedcoll.bytes_read, 1, pBedcoll.file_in);

        collres = (unsigned char *)malloc(pBedcoll.bytes_read);
        std::fill_n(collres, pBedcoll.bytes_read, 0x00);
        for (size_t i = 0; i < bytes_left; i++) {
            collres[i] = collgen[buffer[i] * 256 + buffer[i + bytes_shift]];
            //                using namespace boost;
            //                std::cout << format("%5d  %5d  %5d") % (int)buffer[i] % (int)buffer[i + bytes_shift] % (int)collres[i] << "\n";
        }

        pBedcoll.outfile = fopen(outfn.c_str(), "w+");
        if (pBedcoll.outfile) {
            fwrite(magicbits, 3, 1, pBedcoll.outfile);
            fwrite(collres, pBedcoll.bytes_read, 1, pBedcoll.outfile);
        } else {
            fprintf(stderr, "Failed to open file: %s", outfn.c_str());
            exit(EXIT_FAILURE);
        }

        free(buffer);
        free(collres);
    } else {
        size_t nsnp_read = pBedcoll.nsnp - nshift;
        size_t n_iter = (size_t) nsnp_read / pBedcoll.snp_iter;
        size_t n_remain = nsnp_read % pBedcoll.snp_iter;
        size_t bytes_iter = pBedcoll.snp_iter * pBedcoll.bytes_snp;
        size_t bytes_remain = n_remain * pBedcoll.bytes_snp;
        size_t bytes_shift = nshift * pBedcoll.bytes_snp;
        size_t bytes_all_iters = n_iter * bytes_iter;
        size_t bytes_remain_plus_shift = bytes_remain + bytes_shift;


        pBedcoll.outfile = fopen(outfn.c_str(), "w+");
        if (pBedcoll.outfile) {
            fwrite(magicbits, 3, 1, pBedcoll.outfile);
        } else {
            fprintf(stderr, "Failed to open file: %s", outfn.c_str());
            exit(EXIT_FAILURE);
        }

        res_buffer = (unsigned char *)malloc(bytes_iter);
        buffer = (unsigned char *)malloc(bytes_iter + bytes_shift);

        for (size_t i = 0; i < n_iter; i++) {
            fseeko(pBedcoll.file_in, 3 + i * bytes_iter, SEEK_SET);
            fread(buffer, bytes_iter + bytes_shift, 1, pBedcoll.file_in);
            for (size_t j = 0; j < bytes_iter; j++) {
                res_buffer[j] = collgen[buffer[j] * 256 + buffer[j + bytes_shift]];
            }
            fwrite(res_buffer, bytes_iter, 1, pBedcoll.outfile);
        }

        res_buffer_remain = (unsigned char *)malloc(bytes_remain_plus_shift);
        std::fill_n(res_buffer_remain, bytes_remain_plus_shift, 0x00);
        remain_buffer = (unsigned char *)malloc(bytes_remain_plus_shift);

        fseeko(pBedcoll.file_in, 3 + bytes_all_iters, SEEK_SET);
        fread(remain_buffer, bytes_remain_plus_shift, 1, pBedcoll.file_in);
        for (size_t i = 0; i < bytes_remain; i++) {
            res_buffer_remain[i] = collgen[remain_buffer[i] * 256 + remain_buffer[i + bytes_shift]];
        }
        fwrite(res_buffer_remain, bytes_remain_plus_shift, 1, pBedcoll.outfile);

        free(buffer);
        free(res_buffer);
        free(remain_buffer);
        free(res_buffer_remain);
    }
}




void collapseSeqShift(bedcoll pBedcoll, size_t nshift_start, size_t nshift_end)
{
    if (nshift_start < 1 or nshift_end > pBedcoll.nsnp - 1) {
        perror("Shift number out of range.");
    }
    for (size_t i = nshift_start; i <= nshift_end; i++) {
        collapseSingleShift(pBedcoll, i);
    }
}
