/*
 * rcpparmadillo.h
 *
 *  Created on: Aug 8, 2014
 *      Author: kaiyin
 */

#ifndef RCPPARMADILLO_H_
#define RCPPARMADILLO_H_

#include "RcppArmadillo.h"
// [[Rcpp::depends(RcppArmadillo)]]
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <boost/filesystem.hpp>

#endif /* RCPPARMADILLO_H_ */
