#include "bedcollr.h"                          // bedcollinit() collapseSeqShift()
#include "bedcollc.h"

// [[Rcpp::export]]
void bedcollr(std::string bfile, size_t nshift_min, size_t nshift_max) {
    bedcoll sBed = bedcollinit(bfile, false);
    collapseSeqShift(sBed, nshift_min, nshift_max);
}

// int main(int argc, char *argv[])
// {
//     std::string fn = "/Users/kaiyin/Desktop/mmp2/mmp13";
//     bedcollr(fn, 1, 8);
//     return 0;
// }
//
