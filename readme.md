# Requirements

* git
* R packages:
    * Rcpp
    * devtools
* boost c++ library (important, without this some c++ function won't compile)


# Installation

    require(devtools)
    install_bitbucket("kindlychung/txtutils2")

# Changes

* some printing functions for debugging

# To do
